<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique()->index();
            $table->string('description');
            $table->decimal('total', 11, 2)->unsigned()->default(0);
            $table->date('period')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prks');
    }
}
