<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlAccountDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gl_account_id',
        'period',
        'total'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'gl_account_pos_id' => 'integer',
        'total' => 'decimal'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'period'
    ];

    /**
     * Get the GL Account that owns the Detail.
     */
    public function GlAccount()
    {
        return $this->belongsTo('App\GlAccount');
    }
}
