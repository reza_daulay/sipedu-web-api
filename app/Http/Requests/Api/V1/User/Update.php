<?php

namespace App\Http\Requests\Api\V1\User;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('users');
        return [
            'name'=> 'required',
            'email'=> 'required|email|unique:users,email,'.$id,
			'password'=> 'min:6',
            // 'signature_file'=> 'image|max:'.(\config('user.signature_file.upload_size')/1000),
        ];
    }
}
