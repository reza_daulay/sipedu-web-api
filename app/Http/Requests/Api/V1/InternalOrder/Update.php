<?php

namespace App\Http\Requests\Api\V1\InternalOrder;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		$id = $this->route('internal_orders');
        return [
			'gl_account_pos_id'=> 'required|exists:gl_account_pos,id',
			'code'=> 'required|unique:internal_orders,code,'.$id,
			'description' => 'required'
        ];
    }
}
