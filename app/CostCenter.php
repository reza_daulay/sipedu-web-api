<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostCenter extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gl_account_pos_id',
        'description',
        'code',
        'note'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'gl_account_pos_id' => 'integer',
        'description' => 'string',
        'code' => 'string',
        'note' => 'string'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Get the Pos that owns the GL Account.
     */
    public function Pos()
    {
        return $this->belongsTo('App\GlAccountPos');
    }
}
