<?php

namespace App\Http\Requests\Api\V1\Prk;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'code'=> 'required',
			'description'=> 'required',
            'total'=> 'required|number',
            'period' => 'required:date_format:Y-m-d'
        ];
    }
}
