<?php

namespace App\Http\Requests\Api\V1\CostCenter;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'gl_account_pos_id'=> 'required|exists:gl_account_pos,id',
			'code'=> 'required|unique:cost_centers',
			'description' => 'required'
        ];
    }
}
