<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->namespace('Api\V1')->group(function () {
    Route::middleware('auth:api')->group(function () {
        Route::delete('oauth/token', 'OauthController@logout');
        Route::get('me', 'AccountController@show');
        Route::patch('me', 'AccountController@update');
        Route::apiResource('cost_centers', 'CostCenterController');
        Route::apiResource('gl_accounts', 'GlAccountController');
        Route::apiResource('gl_account_details', 'GlAccountDetailController');
        Route::apiResource('gl_account_pos', 'GlAccountPosController');
        Route::apiResource('internal_orders', 'InternalOrderController');
        Route::apiResource('prk', 'PrkController');
        Route::apiResource('users', 'UserController');
    });
});