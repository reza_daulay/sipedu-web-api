<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlAccountPos extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Get the  of owned GL Account of POS.
     */
    public function GlAccounts()
    {
        return $this->hasMany('App\GlAccount');
    }
}
