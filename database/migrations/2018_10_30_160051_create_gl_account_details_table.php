<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlAccountDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gl_account_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gl_account_id')->unsigned();
			$table->foreign('gl_account_id')->references('id')->on('gl_accounts')->onUpdate('cascade')->onDelete('cascade');
            $table->date('period')->index();
            $table->decimal('total', 11, 2)->unsigned()->default(0);
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gl_account_details');
    }
}
